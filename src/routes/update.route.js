const express = require("express");
const router = express.Router();

// aqui hay que decidir si usar require o import, esto depende de la version de node + babel
// recomiendo require para maxima compatibilidad

import UpdateService from '../services/update.service'; // pendiente cambiar a require
import http from 'http'; // cambiar a require

const updateService = new UpdateService();

router.get('/update', function (req, res) {

    updateService.getPatients().then(data => {
        if (data.code === 401) {
            return res.status(data.code).send(data.message);
        }
        return res.status(201).jsonp(data);
    })
        .catch(error => {
            return res.status(500).send(error.message);
        });

});

module.exports = router;