import express from 'express';
const app = express();

const loginRoutes = require('./routes/update.route');
app.use(loginRoutes);

app.listen(4000, function () {
    console.log('Example app listening on port http://localhost:4000!')
});