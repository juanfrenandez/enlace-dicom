
class TaskResult {

    constructor(response) {

        const { message = '', code = 200, data = {} } = response;
        const error = !response.error;

        return {
            message,
            code,
            data,
            error
        }
    }
};

module.exports = TaskResult;