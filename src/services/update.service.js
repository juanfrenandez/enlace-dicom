const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const rp = require('request-promise'); // check documentation https://github.com/request/request-promise
const TaskResult = require('../commons/task-result');
const { DICOM_API_BASE_URL, DICOM_API_TIMEOUT } = require('../configs.json');

/**
 * Servicio que encapsula todo la comunicacion para extraer los datos del dicom server
 */
class UpdateService { // posiblement puede ser renombrado, ej: UpdateFromDicomService

    constructor() {
    }

    /**
     * Obtiene la lista de pacientes del servidor dicom // estos comentarios ayudan al intelisense de los editores
     */
    getPatients() {

        // usar 'const' o en su defecto 'let'
        const prom = new Promise((fulfill, reject) => {

            const options = {
                uri: `${DICOM_API_BASE_URL}/patients`,
                method: 'GET',
                timeout: DICOM_API_TIMEOUT
            };
            console.log('options: ', options);
            rp(options).then(
                (response) => {

                    const result = JSON.parse(response);
                    // console.log(result[0]); // eliminar codigo console.log antes de hacer push

                    // esta clase 'TaskResult', parece que esta de mas, revisar la logica
                    const tr = new TaskResult({
                        message: 'sin errores',
                        data: result,
                        code: 200,
                        error: false
                    });

                    // posible problema porque estas completando la promesa y aun tienes codigo abajo 
                    //fulfill(tr);

                    // este codigo for ya no es buena practica, 
                    // usar de preferencia forEach, ejemplo abajo
                    // for (var index = 0; index < result.length; index++) {
                    //     var element = result[index];
                    //     //registra usuarios cuando se instala o actualiza la base de datos de los pacientes por licencia. Dropea todos los registros de pacientes
                    //     const register = rp()
                    // }

                    // asi podira quedar
                    if (result && Array.isArray(result)) { // revisamos que exista algo y que sea arreglo
                        result.forEach(element => {
                            //registra usuarios cuando se instala o actualiza la base de datos de los pacientes por licencia. Dropea todos los registros de pacientes
                            const register = rp(); // <--- creo que no estas haciendo nada?
                        });
                    }

                    // la promesa se debe completar al final
                    fulfill(tr);

                })
                .catch((err) => {
                    // confirmo que esta clase puede estar de mas
                    // const tr = new TaskResult({
                    //     message: 'errores 1',
                    //     data: 'nada chido',
                    //     code: 500,
                    //     error: true
                    // });
                    // reject(tr);

                    // este pudo haber sido el codigo...
                    // reject({
                    //     message: 'errores 2',
                    //     data: err,
                    //     code: 500,
                    //     error: true
                    // });

                    // ya tienes el objeto con todos los datos del error
                    console.log('err: ', err);
                    reject(err);
                })

        })
        return prom
    }

}

module.exports = UpdateService;